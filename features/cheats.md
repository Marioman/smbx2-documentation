# Cheats

SMBX2 includes a lot of cheats that are useful for debugging, testing, or just for fun. Cheats can be activated by typing them out while the game is in focus, or by typing them into the tab-activated console. Typing toggled cheats a second time will deactivate them. Activating a cheat will disallow saving in an episode.

## Debug cheats

These cheats are intended for debugging purposes.

| Cheat | Effect |
| --- | --- |
| redigitiscool | Allows the player to save after cheating |
| gdiredigit | Allows the player to save after cheating |
| framerate | Displays the framerate |
| foundmycarkeys | Instantly grants a keyhole exit |
| mylifegoals | Instantly grants a SMW goal tape exit |
| mysteryball | Instantly grants a SMB3 goal orb exit |
| itsvegas | Instantly grants a SMB3 roulette exit |
| getdemstars | Causes the player to teleport around the level, collecting all obtainable stars |
| getmeouttahere | Instantly exits the level |
| hadron | Displays colliders in the level |
| groundhog | Reloads the level from the last warp |
| noclip | Causes the player to move like a cursor (unaffected by gravity or collision) |
| fourthwall | Activates the tab-activated console |

## Gameplay tweak cheats

These cheats toggle some form of gameplay manipulation.

| Cheat | Effect |
| --- | --- |
| newleaf | Deactivates all active cheats |
| donthurtme | Invulnerability |
| wingman | Infinite flight timer for flight powers |
| sonicstooslow | Increases movement speed cap |
| gottagofast | Increases movement speed cap |
| ahippinandahoppin | Infinite jumps |
| jumpman | Infinite jumps |
| speeddemon | Uncaps framerate |
| flamethrower | Removes projectile firing cooldown for players |
| stickyfingers | Allows players to grab any NPC |
| nowiknowhowameatballfeels | Allows Yoshi to eat any NPC |
| moneytree | Gives the player one coin per frame while active |
| captainn | Allows you to toggle time freeze by pressing pause |
| holytrinity | Activates shadowstar, donthurtme, jumpman |
| passerby | Activates sonicstooslow, donthurtme, jumpman |
| theessentials | Activates sonicstooslow, shadowstar, donthurtme, jumpman |
| theessenjls | Activates sonicstooslow, shadowstar, donthurtme, jumpman |
| fromthedepths | Jump high when falling into a pit instead of dying |
| waitinginthesky | Changes the starman music and duration |
| rinkamania | When you kill an enemy, Rinkas spawn |
| jumpforrinka | When you jump, Rinkas spawn |
| rinkamadness | Rinkas spawn randomly |

## Instant effect cheats

These cheats have an instant one-time effect.

| Cheat | Effect |
| --- | --- |
| iceage | Freezes onscreen NPCs |
| wariotime | Turns onscreen NPCs into coins |
| murder | Instakills most onscreen NPCs |
| redrum | Instakills most onscreen NPCs |
| wetwater | Toggles the current section's underwater state (also changes background and music) |
| itsrainingmen | Causes 1-up mushrooms to fall from the sky |
| itsrainingmegan | Causes Megan to fall from the sky |
| andmyaxe | Spawns a bunch of axes all over the screen |
| donttypethis | Causes bombs to fall from the sky |
| istillplaywithlegos | Causes all broken blocks in the level to be restored |
| boomtheroom | Activates a POW effect |
| instantswitch | Activates a P-Switch effect |
| liveforever | Sets life counter to 99 |
| rosebud | Sets coin counter to 99  |
| fairymagic | Turns the player into a fairy |
| thestarmen | Grants the starman effect |
| bitemythumb | Grants a mega mushroom |
| launchme | Instantly propels the player upwards |
| getdown | Instantly shifts the player downwards to the next unoccupied space |
| geddan | Instantly shifts the player downwards to the next unoccupied space |
| horikawaisradicola | Changes all sound effects |
| worldpeace | All effects fly upwards and play weird noises |

## Player cheats

These cheats manipulate the player in some capacity.

| Cheat | Effect |
| --- | --- |
| supermario2 | You can control 2 different characters, switching between pressing down and drop item |
| supermario4 | The player explodes into 4 |
| supermario8 | The player explodes into 8 |
| supermario16 | The player explodes into 16 |
| supermario32 | The player explodes into 32 |
| supermario64 | The player explodes into 64 |
| supermario128 | The player explodes into 128 |
| 1player | Switches to singleplayer mode |
| 2player | Switches to multiplayer mode |
| stophittingme | Damages the player |
| suicide | Instakills the player |
| dressmeup | Equips a random costume for the currently used character |
| undress | Unequips the player's currently used costume, if any |
| laundryday | Unequips all currently used costumes across all characters |

## Character cheats

These cheats change the player character.

| Cheat | Effect |
| --- | --- |
| itsamemario | Changes player to Mario |
| itsameluigi | Changes player to Luigi |
| ibakedacakeforyou | Changes player to Peach |
| itsamepeach | Changes player to Peach |
| anothercastle | Changes player to Toad |
| itsametoad | Changes player to Toad |
| iamerror | Changes player to Link |
| itsamelink | Changes player to Link |
| superfightingrobot | Changes player to Megaman |
| itsamemegaman | Changes player to Megaman |
| itsamegaman | Changes player to Megaman |
| eternalgreed | Changes player to Wario |
| itsamewario | Changes player to Wario |
| kingofthekoopas | Changes player to Bowser |
| itsamebowser | Changes player to Bowser |
| dreamtraveler | Changes player to Klonoa |
| dreamtraveller | Changes player to Klonoa |
| itsameklonoa | Changes player to Klonoa |
| cosmicpower | Changes player to Rosalina |
| itsamerosalina | Changes player to Rosalina |
| metalgear | Changes player to Snake |
| itsamesnake | Changes player to Snake |
| ocarinaoftime | Changes player to Zelda |
| itsamezelda | Changes player to Zelda |
| densenuclearenergy | Changes player to Ultimate Rinka |
| itsameultimaterinka | Changes player to Ultimate Rinka |
| unclesam | Changes player to Uncle Broadsword |
| itsamebroadsword | Changes player to Uncle Broadsword |
| itsameunclebroadsword | Changes player to Uncle Broadsword |
| samusisagirl | Changes player to Samus |
| itsamesamus | Changes player to Samus |
| itsamemetroid | Changes player to Samus |

## Item supply (hand) cheats

These cheats instantly spawn an item in the player's hands.

| Cheat | Effect |
| --- | --- |
| wherearemycarkeys | Spawns a key |
| boingyboing | Spawns a springboard |
| bombsaway | Spawns a bomb |
| firemissiles | Spawns a bullet bill |
| powhammer | Spawns a POW block |
| hammerinmypants | Spawns a hammer |
| rainbowrider | Spawns a flipped rainbow shell |
| upandout | Spawns a propeller block |
| burnthehousedown | Spawns a propeller flamethrower block |
| greenegg | Spawns a green egg containing a yoshi |
| redegg | Spawns a red egg containing a yoshi |
| blueegg | Spawns a blue egg containing a yoshi |
| yellowegg | Spawns a yellow egg containing a yoshi |
| purpleegg | Spawns a purple egg containing a yoshi |
| pinkegg | Spawns a pink egg containing a yoshi |
| coldegg | Spawns a cyan egg containing a yoshi |
| blackegg | Spawns a black egg containing a yoshi |

## Item supply (itembox) cheats

These cheats instead instantly spawn the item in the player's reserve item box.

| Cheat | Effect |
| --- | --- |
| needashell | Spawns a green shell |
| needaredshell | Spawns a red shell |
| needablueshell | Spawns a blue shell |
| needayellowshell | Spawns a yellow shell |
| needaturnip | Spawns a turnip |
| needa1up | Spawns a 1-up mushroom |
| needamoon | Spawns a 3-up moon |
| needatanookisuit | Spawns a tanooki suit |
| needahammersuit | Spawns a hammer suit |
| needamushroom | Spawns a mushroom |
| needaflower | Spawns a fire flower |
| needaniceflower | Spawns an ice flower |
| needaleaf | Spawns a leaf |
| needanegg | Spawns an empty yoshi egg |
| needaplant | Spawns a toothy plant |
| needagun | Spawns a billy gun |
| needaswitch | Spawns a p-switch |
| needaclock | Spawns a timestop watch |
| needabomb | Spawns a bomb |
| needashoe | Spawns a kuribo's shoe |
| needaredshoe | Spawns a podoboo's shoe |
| needablueshoe | Spawns a lakitu's shoe |

## World map cheats

These cheats are effective on the world map

| Cheat | Effect |
| --- | --- |
| imtiredofallthiswalking | Unlocks all paths |
| illparkwhereiwant | Allows the player to move anywhere |
