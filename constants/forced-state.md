# Player forced state constants

Constants for player.forcedState values. Note that some may be unsafe to set. Also, there are no states for powering down from 3 hearts with any other powerup.

| Constant | Value | Description |
| --- | --- | --- |
| FORCEDSTATE_NONE | 0 | No forced state. |
| FORCEDSTATE_POWERUP_BIG | 1 | Powering up to big state. |
| FORCEDSTATE_POWERDOWN_SMALL | 2 | Powering down to small state. |
| FORCEDSTATE_PIPE | 3 | Entering a pipe (crashes if no valid warp is provided). |
| FORCEDSTATE_POWERUP_FIRE | 4 | Powering up to fire state. |
| FORCEDSTATE_POWERUP_LEAF | 5 | Powering up to leaf state. |
| FORCEDSTATE_RESPAWN | 6 | Respawning in Multiplayer. Known to be glitchy when manipulated. |
| FORCEDSTATE_DOOR | 7 | Entering a door (crashes if no valid warp is provided). |
| FORCEDSTATE_INVISIBLE | 8 | Invisibility while transforming into a fairy or different character. |
| FORCEDSTATE_ONTONGUE | 9 | On another player's yoshi's tongue. |
| FORCEDSTATE_SWALLOWED | 10 | Swallowed by another player's yoshi. |
| FORCEDSTATE_POWERUP_TANOOKI | 11 | Powering up to tanooki state. |
| FORCEDSTATE_POWERUP_HAMMER | 12 | Powering up to hammer state. |
| FORCEDSTATE_POWERUP_ICE | 41 | Powering up to ice state. |
| FORCEDSTATE_POWERDOWN_FIRE | 227 | Powering down from fire while at 3 hearts. |
| FORCEDSTATE_POWERDOWN_ICE | 228 | Powering down from ice while at 3 hearts. |
| FORCEDSTATE_MEGASHROOM | 499 | Mega shroom transformation state. |
| FORCEDSTATE_TANOOKI_POOF | 500 | Tanooki statue transformation poof. |