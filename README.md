# SMBX2 Documentation

Welcome to the SMBX2 documentation page.

[Get Started](/general/introduction.md)

[Download SMBX2](/general/installation.md)

[The Editor](/general/editor.md)

Use the search field or the list on the left to find the documentation you are looking for.

The documentation is actively developed by the SMBX2 development team and contributors like you! If you are noticing something wrong or missing, feel free to help out.

[Contributing to the docs](/guides/contributing-to-the-docs.md)