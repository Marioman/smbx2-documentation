# Object

Lua is not an object-oriented programming language. All classes and objects built within lua are specialized applications of [tables](/types/table.md).

When used in this documentation, the term object refers to a nonspecific return value that changes depending on the input to a function. Most commonly, this is the case for a class's generic mem() function.